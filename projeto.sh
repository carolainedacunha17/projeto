#!/bin/bash

verificar_servidor_ssh() {
  dpkg -s openssh-server &> /dev/null
  return $?
}

instalar_servidor_ssh() {
  sudo apt-get update
  sudo apt-get install -y openssh-server
}

selecionar_arquivo_origem() {
  arquivo_origem=$(zenity --file-selection --title="Selecione o arquivo de origem")
  if [[ -z "$arquivo_origem" ]]; then
    zenity --error --text="Nenhum arquivo de origem selecionado."
    exit 1
  fi
}

selecionar_destino() {
  destino=$(zenity --entry --title="Selecione o destino" --text="Digite o destino (usuário@host:/caminho):")
  if [[ -z "$destino" ]]; then
    zenity --error --text="Nenhum destino selecionado."
    exit 1
  fi
}

inserir_endereco_ssh() {
	endereco_ssh=$(zenity --entry --title="Servidor SSH" --text="Digite o endereço do servidor SSH:")
  if [[ -z "$endereco_ssh" ]]; then
    zenity --error --text="Nenhum endereço de servidor SSH fornecido."
    exit 1
  fi
}

copiar_arquivo() {
  local origem="$1"
  local destino="$2"

  scp "$origem" "$destino"

  if [[ $? -eq 0 ]]; then
    zenity --info --text="Cópia de arquivo bem-sucedida."
  else
    zenity --error --text="Falha na cópia do arquivo."
  fi
}

verificar_md5() {
  local arquivo="$1"
  local md5_origem=$(md5sum "$arquivo" | awk '{print $1}')
  local md5_destino=$(ssh "$endereco_ssh" "md5sum $destino" | awk '{print $1}')

  if [[ "$md5_origem" == "$md5_destino" ]]; then
    zenity --info --text="Verificação MD5 bem-sucedida. O arquivo foi copiado corretamente."
  else
    zenity --error --text="Verificação MD5 falhou. O arquivo pode ter sido corrompido durante a cópia."
  fi
}

exibir_lista_arquivos() {
  local endereco_ssh="$1"

  local arquivo_locais=$(ls)

  local arquivos_remotos=$(ssh "$endereco_ssh" ls)

  zenity --info --title="Lista de Arquivos" --text="Arquivos locais:
$arquivo_locais

Arquivos remotos ($endereco_ssh):
$arquivos_remotos"
}

main() {
  if ! verificar_servidor_ssh; then
    zenity --question --text="O servidor SSH não está instalado. Deseja instalá-lo agora?"
    if [[ $? -eq 0 ]]; then
      instalar_servidor_ssh
    else
      exit 1
    fi
  fi

  acao=$(zenity --list --title="Menu" --column="Ação" \
    "Copiar arquivo do cliente para o servidor" \
    "Copiar arquivo do servidor para o cliente" \
    "Visualizar lista de arquivos no cliente e no servidor")

  case "$acao" in
    "Copiar arquivo do cliente para o servidor")
      selecionar_arquivo_origem
      inserir_endereco_ssh
      selecionar_destino
      copiar_arquivo "$arquivo_origem" "$destino"
      verificar_md5 "$arquivo_origem"
      ;;
    "Copiar arquivo do servidor para o cliente")
      inserir_endereco_ssh
      selecionar_arquivo_origem
      copiar_arquivo "$endereco_ssh:$arquivo_origem" "."
      verificar_md5 "$arquivo_origem"
      ;;
    "Visualizar lista de arquivos no cliente e no servidor")
      inserir_endereco_ssh
      exibir_lista_arquivos "$endereco_ssh"
      ;;
    *)
      zenity --error --text="Ação inválida selecionada."
      exit 1
      ;;
  esac
}

main


